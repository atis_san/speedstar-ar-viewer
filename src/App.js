
import React, { Suspense, useRef, useEffect, useState } from 'react';
import './App.scss';
import { Canvas, useFrame, useThree } from "@react-three/fiber";
import { Plane, Sphere, softShadows, OrbitControls, useGLTF, useAnimations, Box } from '@react-three/drei';
import { ARCanvas, DefaultXRControllers, Interactive, useHitTest } from '@react-three/xr';

import {
  BrowserRouter as Router,
  useLocation
} from "react-router-dom";

function useQuery() {
  const { search } = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
}

softShadows();


const Horse = ({ ...props }) => {
  const { horseID } = props
  const horseRef = useRef(null)
  const { nodes, materials, animations } = useGLTF(`https://cdn.speedstargame.com/horses/develop/${horseID}.glb`)
  const { actions } = useAnimations(animations, horseRef)

  useEffect(() => {
    actions.idle.play();
  }, [])

  const keys = Object.keys(nodes);

  var count = 0;
  return (
    <>
      <group ref={horseRef} {...props} dispose={null}>
        <primitive object={nodes} />
        {
          keys.map((k, index) => {
            if (nodes[k].type !== "SkinnedMesh")
              return null
            else {
              const matName = `${k.replace("_mesh", "")}_material01`
              console.log("k" + matName)

              const rootName = (count === 0) ? 'ROOT' : `ROOT_${count}`
              count++;
              console.log(rootName)
              return (
                <>
                  <primitive object={nodes[rootName]} />
                  <skinnedMesh castShadow
                    geometry={nodes[`${k}`].geometry}
                    material={materials[matName]}
                    skeleton={nodes[`${k}`].skeleton}
                  />
                </>
              )
            }
          })
        }
        {/* <skinnedMesh castShadow
          geometry={nodes.hair_031_mesh.geometry}
          material={materials.hair_031_material01}
          skeleton={nodes.hair_031_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_1} />
        <skinnedMesh castShadow
          geometry={nodes.mane_001_mesh.geometry}
          material={materials.mane_001_material01}
          skeleton={nodes.mane_001_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_2} />
        <skinnedMesh castShadow
          geometry={nodes.tail_031_mesh.geometry}
          material={materials.tail_031_material01}
          skeleton={nodes.tail_031_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_3} />
        <skinnedMesh castShadow
          geometry={nodes.hoof_001_mesh.geometry}
          material={materials.hoof_001_material01}
          skeleton={nodes.hoof_001_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_4} />
        <skinnedMesh castShadow
          geometry={nodes.body_001_mesh.geometry}
          material={materials.body_001_material01}
          skeleton={nodes.body_001_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_5} />
        <skinnedMesh castShadow
          geometry={nodes.ear_098_mesh.geometry}
          material={materials.ear_098_material01}
          skeleton={nodes.ear_098_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_6} />
        <skinnedMesh castShadow
          geometry={nodes.leg_001_mesh.geometry}
          material={materials.leg_001_material01}
          skeleton={nodes.leg_001_mesh.skeleton}
        />
        <primitive object={nodes.ROOT_7} />
        <skinnedMesh castShadow
          geometry={nodes.eye_070_mesh.geometry}
          material={materials.eye_070_material01}
          skeleton={nodes.eye_070_mesh.skeleton}
        /> */}
      </group>
    </>
  )
}

function App(props) {

  // const [bearAction, setBearAction] = useState("idle");
  const sides = 20;

  let query = useQuery();
  const id = query.get("id");

  useEffect(() => {

  }, [])

  return (
    <>
      <ARCanvas
        sessionInit={{ requiredFeatures: ['hit-test'] }}
        shadows
        colorManagement
        camera={{ position: [0, 0.0, -10], fov: 35 }}>
        <ambientLight intensity={0.8} />
        <directionalLight position={[0, 10, 0]} intensity={1}
          castShadow
          shadow-mapSize-height={2048}
          shadow-mapSize-width={2048}
          shadow-camera-far={50}
          shadow-camera-near={0.5}
          shadow-camera-left={-sides}
          shadow-camera-right={sides}
          shadow-camera-top={sides}
          shadow-camera-bottom={-sides}
        />

        <group position={[0, -1.5, 0]}>

          <Suspense fallback={null}>
            <Horse horseID={id} action={"idle"} scale={[0.5, 0.5, 0.5]} position={[0, 0, 0]} rotation={[0, 145 * Math.PI / 180, 0]}></Horse>
          </Suspense>
          <mesh receiveShadow rotation={[-Math.PI / 2, 0, 0]} position={[0, 0, 0]}>
            <planeBufferGeometry attach='geometry' args={[100, 100]} />
            <shadowMaterial attach="material" opacity={0.3} />
          </mesh>
        </group>
        <DefaultXRControllers />
        <OrbitControls />
      </ARCanvas>
    </>
  );
}

export default App;
